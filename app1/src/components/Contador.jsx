import React from 'react'

const Contador = () => {

	const [contador, setContador] = React.useState(0)

	return (
		<>
			<h3>Contador: <label>{contador}</label></h3>
			<button onClick={ () => setContador(contador + 1) }>Aumentar</button>
		</>
	)
}

export default Contador