import React from 'react'

const Variables = () => {

	const saludo = "Hola desde constante"
	const imagen = "https://www.infobae.com/new-resizer/wPGdqxLdYbV_mmhszcvKRt9863g=/768x432/filters:format(jpg):quality(85)/cloudfront-us-east-1.images.arcpublishing.com/infobae/O7CMACS2YBCUXHQYJ74TGW5J5I.jpg"
	return (
		<div>
			<h2>Nuevo componentes { saludo }</h2>
			<img src={ imagen } alt=""/>
		</div>
	)
}

export default Variables