import React, { useState } from 'react'

const Listas = () => {

	const arrayInicial = [1,2,3,4,5];
	const arrayObjetos = [
		{id: 1, texto: 'tarea 1'},
		{id: 2, texto: 'tarea 2'},
		{id: 3, texto: 'tarea 3'},
	];
	const [lista, setLista] = useState(arrayInicial);
	const [lista2, setLista2] = useState(arrayObjetos);

	const agregarElementos = () => {
		console.log('click')
		let contador = lista2.length + 1;
		setLista2([
			...lista2,
			{id: contador, texto: 'tarea '+contador}
			]);
	}
	return (
		<div>
			<h2>Listas</h2>
			{
				lista.map( (item, index) => (
					<h4 key={index} >{item}</h4>
				))
			}
			<h2>Listas 2</h2>
			{
				lista2.map( (item, index) => (
					<h4 key={item.id} >{item.texto}</h4>
				))
			}
			<button onClick={ () => agregarElementos() }>Agregar</button>
		</div>
	)
}

export default Listas