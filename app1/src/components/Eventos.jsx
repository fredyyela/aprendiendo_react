import React, { Fragment, useState } from 'react'

const Eventos = () => {

	const [texto, setTexto] = useState('Hola soy el texto original')
	//const [active, setActive] = useState(1)

	const eventoClick = () => {
		console.log('me diste un click')
		setTexto('hola soy el nuevo texto')
	}

	return (
		<Fragment>
			<hr />
			<h2>Eventos</h2>
			<p> { texto }</p>
			<button onClick={ () => eventoClick() }>Click</button>
		</Fragment>
	)
}

export default Eventos